import { cloudinaryLink } from 'consts';

export const getImage = link => (cloudinaryLink + link);

export default { getImage };
