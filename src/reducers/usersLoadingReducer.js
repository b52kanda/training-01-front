import { FETCH_USERS_START, FETCH_USERS_STOP } from 'actions/types';

export default function usersLoadingReducer(state = false, action) {
  switch (action.type) {
    case FETCH_USERS_START:
      return true;
    case FETCH_USERS_STOP:
      return false;
    default:
      return state;
  }
}
