import { FETCH_LAST_COMPLETED_MATCH_INFO, INCREMENT_PLAYER_VOTES } from 'actions/types';

const defaultState = {
  teamNames: [],
  allPlayers: [],
  matchId: '',
};

export default function lastCompletedMatchInfoReducer(state = defaultState, action) {
  switch (action.type) {
    case FETCH_LAST_COMPLETED_MATCH_INFO:
      return action.lastCompletedMatchInfo;
    case INCREMENT_PLAYER_VOTES:
      return Object.assign({}, state, {
        allPlayers: [...state.allPlayers].map((player) => {
          if (player.player._id === action.player._id) {
            return Object.assign({}, player, { player: action.player });
          }

          return player;
        }),
      });
    default:
      return state;
  }
}
