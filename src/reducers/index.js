import { combineReducers } from 'redux';
import user from './userReducer';
import users from './usersReducer';
import usersLoading from './usersLoadingReducer';
import notifications from './notificationsReducer';
import userLoading from './userLoadingReducer';
import lastCompletedMatchInfo from './lastCompletedMatchInfoReducer';
import lastCompletedMatchInfoLoading from './lastCompletedMatchInfoLoadingReducer';
import matchesResults from './matchesResultsReducer';
import matchesResultsLoading from './matchesResultsLoadingReducer';

export default combineReducers({
  user,
  users,
  usersLoading,
  notifications,
  userLoading,
  lastCompletedMatchInfo,
  lastCompletedMatchInfoLoading,
  matchesResults,
  matchesResultsLoading,
});
