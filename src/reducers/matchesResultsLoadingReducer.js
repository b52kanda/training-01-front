import {
  FETCH_MATCHES_RESULTS_ACTION_START,
  FETCH_MATCHES_RESULTS_ACTION_STOP,
} from 'actions/types';

export default function matchesResultsLoadingReducer(state = true, action) {
  switch (action.type) {
    case FETCH_MATCHES_RESULTS_ACTION_START:
      return true;
    case FETCH_MATCHES_RESULTS_ACTION_STOP:
      return false;
    default:
      return state;
  }
}
