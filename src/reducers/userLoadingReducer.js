import { USER_LOADING_START, USER_LOADING_STOP } from 'actions/types';

export default function userLoadingReducer(state = false, action) {
  switch (action.type) {
    case USER_LOADING_START:
      return true;
    case USER_LOADING_STOP:
      return false;
    default:
      return state;
  }
}
