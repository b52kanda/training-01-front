import { ADD_NOTIFICATION, REMOVE_NOTIFICATION } from 'actions/types';

export default function userReducer(state = [], action) {
  switch (action.type) {
    case ADD_NOTIFICATION:
      return [...state, action.notification];
    case REMOVE_NOTIFICATION:
      return state.filter(({ id }) => id !== action.notification.id);
    default:
      return state;
  }
}
