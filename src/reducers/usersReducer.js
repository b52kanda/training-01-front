import { FETCH_USERS, ACTIVATE_USER, START_USER_UPDATING } from 'actions/types';

export default function usersReducer(state = [], action) {
  switch (action.type) {
    case FETCH_USERS:
      return action.users;
    case ACTIVATE_USER:
      return state.map(user => (user._id !== action.user._id ? user : action.user));
    case START_USER_UPDATING:
      return state.map(user => ({
        ...user,
        isUpdating: user._id === action.user.id,
      }));
    default:
      return state;
  }
}
