import {
  FETCH_LAST_COMPLETED_MATCH_INFO_START,
  FETCH_LAST_COMPLETED_MATCH_INFO_STOP,
} from 'actions/types';

export default function lastCompletedMatchLoadingReducer(state = true, action) {
  switch (action.type) {
    case FETCH_LAST_COMPLETED_MATCH_INFO_START:
      return true;
    case FETCH_LAST_COMPLETED_MATCH_INFO_STOP:
      return false;
    default:
      return state;
  }
}
