import { FETCH_MATCHES_RESULTS_ACTION } from 'actions/types';

export default function matchesResultsReducer(state = [], action) {
  switch (action.type) {
    case FETCH_MATCHES_RESULTS_ACTION:
      return action.matches;
    default:
      return state;
  }
}
