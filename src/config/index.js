import dev from './dev';
import prod from './prod';
import local from './local';

const getConfig = () => {
  if (process.env.REACT_APP_ENV === 'dev') {
    return dev;
  }

  if (process.env.REACT_APP_ENV === 'local') {
    return local;
  }

  return prod;
};

export default getConfig();
