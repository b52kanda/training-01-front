import http from 'utils/http';
import { openErrorNotification } from 'actions/notificationActions';
import {
  FETCH_USERS,
  ACTIVATE_USER,
  START_USER_UPDATING,
  FETCH_USERS_START,
  FETCH_USERS_STOP,
} from './types';

export const fetchUsersAction = users => ({
  type: FETCH_USERS,
  users,
});

export const fetchUsersStartAction = () => ({ type: FETCH_USERS_START });
export const fetchUsersStopAction = () => ({ type: FETCH_USERS_STOP });

export const startUserUpdating = user => ({
  type: START_USER_UPDATING,
  user,
});

export const activateUserAction = user => ({
  type: ACTIVATE_USER,
  user,
});

export const getUsers = () => async (dispatch) => {
  try {
    dispatch(fetchUsersStartAction());
    const { users } = await http.get('/user');

    dispatch(fetchUsersAction(users));
  } catch (error) {
    dispatch(openErrorNotification(error.message));
    console.warn(error.message);
  } finally {
    dispatch(fetchUsersStopAction());
  }
};

export const activateUser = user => async (dispatch) => {
  try {
    dispatch(startUserUpdating(user));
    const response = await http.post('/user/activate', { user });

    dispatch(activateUserAction(response.user));
  } catch (error) {
    dispatch(openErrorNotification(error.message));
    console.warn(error.message);
  }
};
