import http from 'utils/http';
import { openErrorNotification } from 'actions/notificationActions';
import {
  FETCH_MATCHES_RESULTS_ACTION,
  FETCH_MATCHES_RESULTS_ACTION_START,
  FETCH_MATCHES_RESULTS_ACTION_STOP,
} from './types';

export const fetchMatchesResultsAction = matches => ({
  type: FETCH_MATCHES_RESULTS_ACTION,
  matches,
});

export const fetchMatchesResultsActionStart = () => ({
  type: FETCH_MATCHES_RESULTS_ACTION_START,
});

export const fetchMatchesResultsActionStop = () => ({
  type: FETCH_MATCHES_RESULTS_ACTION_STOP,
});

export const getAllMatchesResults = () => async (dispatch) => {
  try {
    dispatch(fetchMatchesResultsActionStart());

    const { completedMatchesResults } = await http.get('/match/results');

    dispatch(fetchMatchesResultsAction(completedMatchesResults));
  } catch (error) {
    dispatch(openErrorNotification(error.message));
  } finally {
    dispatch(fetchMatchesResultsActionStop());
  }
};
