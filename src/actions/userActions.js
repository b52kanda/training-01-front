import http from 'utils/http';
import {
  ADD_USER, REMOVE_USER, USER_LOADING_START, USER_LOADING_STOP,
} from './types';

export const userLoadingStart = () => ({ type: USER_LOADING_START });
export const userLoadingStop = () => ({ type: USER_LOADING_STOP });

export const addUser = user => ({
  type: ADD_USER,
  user,
});

export const removeUser = () => ({
  type: REMOVE_USER,
});

export const loginUser = userRaw => async (dispatch) => {
  try {
    dispatch(userLoadingStart());
    const { user } = await http.post('/user/login', userRaw);

    localStorage.setItem('id_token', user.token);
    dispatch(addUser(user));
  } catch (e) {
    console.warn(e);
  } finally {
    dispatch(userLoadingStop());
  }
};

export const registerUser = (userRaw, history) => async (dispatch) => {
  try {
    dispatch(userLoadingStart());
    await http.post('/user', userRaw);

    history.push('/login');
  } catch (e) {
    console.warn(e);
  } finally {
    dispatch(userLoadingStop());
  }
};

export const logoutUser = () => async (dispatch) => {
  localStorage.removeItem('id_token');
  dispatch(removeUser());
};

export const resetPassword = userRaw => async (dispatch) => {
  try {
    dispatch(userLoadingStart());
    await http.post('/user/resetPassword', userRaw);
  } catch (e) {
    console.warn(e);
  } finally {
    dispatch(userLoadingStop());
  }
};

export const getCurrentUser = () => async (dispatch) => {
  try {
    dispatch(userLoadingStart());
    const response = await http.get('/user/current');

    dispatch(addUser(response.user));
  } catch (e) {
    console.warn(e);
  } finally {
    dispatch(userLoadingStop());
  }
};
