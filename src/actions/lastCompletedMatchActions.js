import http from 'utils/http';
import { openErrorNotification } from 'actions/notificationActions';
import {
  FETCH_LAST_COMPLETED_MATCH_INFO,
  FETCH_LAST_COMPLETED_MATCH_INFO_START,
  FETCH_LAST_COMPLETED_MATCH_INFO_STOP,
  INCREMENT_PLAYER_VOTES,
} from './types';

export const fetchLastCompletedMatchInfoAction = lastCompletedMatchInfo => ({
  type: FETCH_LAST_COMPLETED_MATCH_INFO,
  lastCompletedMatchInfo,
});

export const fetchLastCompletedMatchInfoStartAction = () => ({
  type: FETCH_LAST_COMPLETED_MATCH_INFO_START,
});

export const fetchLastCompletedMatchInfoStopAction = () => ({
  type: FETCH_LAST_COMPLETED_MATCH_INFO_STOP,
});

export const incrementPlayerVotes = player => ({
  type: INCREMENT_PLAYER_VOTES,
  player,
});

export const getLastCompletedMatchInfo = () => async (dispatch) => {
  try {
    dispatch(fetchLastCompletedMatchInfoStartAction());

    const { lastCompletedMatchInfo } = await http.get('/match/lastCompleted');

    dispatch(fetchLastCompletedMatchInfoAction(lastCompletedMatchInfo));
  } catch (error) {
    dispatch(openErrorNotification(error.message));
  } finally {
    dispatch(fetchLastCompletedMatchInfoStopAction());
  }
};

export const addVoteByPlayerAndMatchIds = (playerId, matchId) => async (dispatch) => {
  try {
    const { player } = await http.put(`/player/vote/${playerId}/${matchId}`);

    dispatch(incrementPlayerVotes(player));
  } catch (error) {
    dispatch(openErrorNotification(error.message));
  }
};
