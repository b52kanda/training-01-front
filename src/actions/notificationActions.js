import uuid from 'uuid/v1';
import { ADD_NOTIFICATION, REMOVE_NOTIFICATION } from './types';

export const openNotificationAction = notification => ({
  type: ADD_NOTIFICATION,
  notification,
});

export const closeNotificationAction = notification => ({
  type: REMOVE_NOTIFICATION,
  notification,
});

export const closeNotification = notification => (dispatch) => {
  dispatch(closeNotificationAction(notification));
};

export const openSuccessNotification = message => (dispatch) => {
  dispatch(openNotificationAction({
    message,
    id: uuid(),
    type: 'success',
  }));
};

export const openWarningNotification = message => (dispatch) => {
  dispatch(openNotificationAction({
    message,
    id: uuid(),
    type: 'warning',
  }));
};

export const openErrorNotification = message => (dispatch) => {
  dispatch(openNotificationAction({
    message,
    id: uuid(),
    type: 'error',
  }));
};

export const openInfoNotification = message => (dispatch) => {
  dispatch(openNotificationAction({
    message,
    id: uuid(),
    type: 'info',
  }));
};
