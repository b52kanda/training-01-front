import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({
  component: Component, user, userLoading, allowedRoles, ...rest
}) => {
  const isAllowedUser = userLoading || allowedRoles.includes(user && user.role);

  return (
    <Route
      {...rest}
      render={props => (
        isAllowedUser ? (
          <Component {...props} />
        ) : (
          <Redirect to="/" />
        )
      )}
    />
  );
};

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  allowedRoles: PropTypes.arrayOf(PropTypes.string).isRequired,
  user: PropTypes.shape({ role: PropTypes.string }),
  userLoading: PropTypes.bool.isRequired,
};

PrivateRoute.defaultProps = {
  user: {},
};

export default PrivateRoute;
