import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import MenuItem from '@material-ui/core/MenuItem';

import styles from './MenuItemLink.scss';

const MenuItemLink = ({ to, onClick, children }) => (
  <Link to={to} className={styles.menuItemLink}>
    <MenuItem
      onClick={onClick}
    >
      { children }
    </MenuItem>
  </Link>
);

MenuItemLink.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
};

MenuItemLink.defaultProps = {
  onClick: () => {},
};

export default MenuItemLink;
