import React from 'react';
import { Route, Switch } from 'react-router-dom';

import PrivateRoute from 'containers/PrivateRoute';

import Home from 'pages/home/Home';
import Login from 'pages/authorization/login/Login';
import Admin from 'pages/admin/Admin';
import NoMatch from 'pages/noMatch/NoMatch';
import Registration from 'pages/authorization/registration/Registration';
import RestorePassword from 'pages/authorization/restore/RestorePassword';

import { userAdmin } from 'consts';

export default () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/login" component={Login} />
    <Route path="/registration" component={Registration} />
    <Route path="/restore" component={RestorePassword} />
    <PrivateRoute path="/admin" allowedRoles={[userAdmin]} component={Admin} />
    <Route component={NoMatch} />
  </Switch>
);
