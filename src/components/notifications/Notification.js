import React from 'react';
import PropTypes from 'prop-types';
import Snackbar from '@material-ui/core/Snackbar';
import { withStyles } from '@material-ui/core/styles';
import NotificationContent from './notificationContent/NotificationContent';

const styles = theme => ({
  margin: {
    margin: theme.spacing.unit,
  },
});

const Notifications = (props) => {
  const { classes, notification, onCloseNotification } = props;
  const open = true;

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      open={open}
      autoHideDuration={5000}
      onClose={() => onCloseNotification(notification)}
    >
      <NotificationContent
        className={classes.margin}
        type={notification.type}
        message={notification.message}
        onClose={() => onCloseNotification(notification)}
      />
    </Snackbar>
  );
};

Notifications.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  notification: PropTypes.shape({
    message: PropTypes.string,
  }).isRequired,
  onCloseNotification: PropTypes.func,
};

Notifications.defaultProps = {
  onCloseNotification: () => {},
};

export default withStyles(styles)(Notifications);
