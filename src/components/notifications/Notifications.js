import React from 'react';
import PropTypes from 'prop-types';
import Notification from './Notification';

const Notifications = (props) => {
  const { notifications, onCloseNotification } = props;

  return (
    <div>
      {notifications.map(notification => (
        <Notification
          key={notification.id}
          notification={notification}
          onCloseNotification={(...args) => onCloseNotification(...args)}
        />
      ))}
    </div>
  );
};

Notifications.propTypes = {
  notifications: PropTypes.arrayOf(PropTypes.object),
  onCloseNotification: PropTypes.func,
};

Notifications.defaultProps = {
  notifications: [],
  onCloseNotification: () => {},
};

export default Notifications;
