import React, { PureComponent } from 'react';
import Popover from '@material-ui/core/Popover/Popover';
import PropTypes from 'prop-types';

class CompletedMatchesPopup extends PureComponent {
  constructor(props) {
    super(props);
    this.closePopup = this.closePopup.bind(this);
  }

  closePopup() {
    const { onClosePopup } = this.props;
    onClosePopup();
  }

  render() {
    const { isOpen, children } = this.props;

    return (
      <Popover
        id="completedMatches-popup"
        open={isOpen}
        onClose={this.closePopup}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'center',
          horizontal: 'right',
        }}
      >
        {children}
      </Popover>
    );
  }
}

CompletedMatchesPopup.propTypes = {
  isOpen: PropTypes.bool,
  onClosePopup: PropTypes.func,
  children: PropTypes.element.isRequired,
};

CompletedMatchesPopup.defaultProps = {
  isOpen: false,
  onClosePopup: () => {},
};

export default CompletedMatchesPopup;
