import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MatchResult from 'components/home/recentsMatches/matchesResultsList/matchResult/MatchResult';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import { withNamespaces } from 'react-i18next';
import styles from './allMatchesResultsList.scss';

const AllMatchesResultsList = (props) => {
  const { allMatchesList, t } = props;

  return (
    <div className={styles.wrapper}>
      <AppBar elevation={0} position="static">
        <Toolbar>
          <Typography variant="h6" color="inherit">
            {t('labels.resultAllMatchesLabel')}
          </Typography>
        </Toolbar>
      </AppBar>
      <div className={styles.list}>
        <Scrollbars
          renderView={prop => <div {...prop} />}
          autoHide
        >
          {
            allMatchesList.map(match => (
              <MatchResult
                key={match._id}
                match={match}
              />
            ))
          }
        </Scrollbars>
      </div>
    </div>
  );
};

AllMatchesResultsList.propTypes = {
  t: PropTypes.func,
  allMatchesList: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      homePlayingTeam: PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
        linkToLogo: PropTypes.string,
        goals: PropTypes.number,
      }),
      guestTeam: PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
        linkToLogo: PropTypes.string,
        goals: PropTypes.number,
      }),
    }),
  ),
};

AllMatchesResultsList.defaultProps = {
  t: () => {},
  allMatchesList: [],
};

export default withNamespaces('translation')(AllMatchesResultsList);
