import React from 'react';
import MatchResult from 'components/home/recentsMatches/matchesResultsList/matchResult/MatchResult';
import { withNamespaces } from 'react-i18next';
import PropTypes from 'prop-types';
import Loader from 'components/loader/Loader';
import Typography from '@material-ui/core/Typography';
import styles from './matchesResultsList.scss';

const MatchesResultsList = (props) => {
  const { matchesList, t, matchesListLoading } = props;

  if (matchesListLoading) {
    return (
      <div className={styles.listWrapper}>
        <Loader size="lg" />
      </div>
    );
  }

  if (matchesList.length === 0) {
    return (
      <div className={styles.listWrapper}>
        <Typography variant="subtitle1">
          {t('labels.emptyMatchesListLabel')}
        </Typography>
      </div>
    );
  }

  return (
    <div className={styles.listWrapper}>
      {
        matchesList.map(match => (
          <MatchResult
            key={match._id}
            match={match}
          />
        ))
      }
    </div>
  );
};

MatchesResultsList.propTypes = {
  t: PropTypes.func,
  matchesList: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      homePlayingTeam: PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
        linkToLogo: PropTypes.string,
        goals: PropTypes.number,
      }),
      guestTeam: PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
        linkToLogo: PropTypes.string,
        goals: PropTypes.number,
      }),
    }),
  ).isRequired,
  matchesListLoading: PropTypes.bool,
};

MatchesResultsList.defaultProps = {
  t: () => {},
  matchesListLoading: false,
};

export default withNamespaces('translation')(MatchesResultsList);
