import React from 'react';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import { getImage } from 'services/ImageService';
import styles from './matchResult.scss';

const MatchResult = (props) => {
  const { match } = props;

  const opponent = team => (
    <div className={styles.opponent}>
      <img src={getImage(team.linkToLogo)} alt="logo" />
      <Typography variant="subtitle1" className={styles.name}>
        {team.name}
      </Typography>
    </div>
  );

  return (
    <div key={match._id} className={styles.wrapper}>
      {opponent(match.homePlayingTeam)}
      <div className={styles.count}>
        {`${match.homePlayingTeam.goals}:${match.guestTeam.goals}`}
      </div>
      {opponent(match.guestTeam)}
    </div>
  );
};

MatchResult.propTypes = {
  match: PropTypes.shape({
    _id: PropTypes.string,
    homePlayingTeam: PropTypes.shape({
      _id: PropTypes.string,
      name: PropTypes.string,
      linkToLogo: PropTypes.string,
      goals: PropTypes.number,
    }),
    guestTeam: PropTypes.shape({
      _id: PropTypes.string,
      name: PropTypes.string,
      linkToLogo: PropTypes.string,
      goals: PropTypes.number,
    }),
  }).isRequired,
};

export default MatchResult;
