import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { withNamespaces } from 'react-i18next';
import PropTypes from 'prop-types';
import AllMatchesResultsList from 'components/home/recentsMatches/completedMatchesPopup/allMatchesResults/AllMachesResultsList';
import MatchesResultsList from 'components/home/recentsMatches/matchesResultsList/MatchesResultsList';
import styles from './recentsMatches.scss';
import CompletedMatchesPopup from './completedMatchesPopup/CompletedMatchesPopup';

class RecentsMatches extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPopupOpen: false,
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
  }

  componentWillMount() {
    const { onGetAllMatchesResult } = this.props;
    onGetAllMatchesResult();
  }

  handleOpen() {
    this.setState({
      isPopupOpen: true,
    });
  }

  handleClose() {
    this.setState({
      isPopupOpen: false,
    });
  }

  render() {
    const {
      t,
      recentsMatchesList,
      matchesResultsLoading,
      allMatchesResultsList,
    } = this.props;

    const { isPopupOpen } = this.state;

    return (
      <div className={styles.wrapper}>
        <Paper square elevation={1}>
          <AppBar elevation={0} position="static">
            <Toolbar className={styles.toolbar}>
              <Typography variant="h6" color="inherit">
                {t('labels.recentsMatchesLabel')}
              </Typography>
              <Button
                color="inherit"
                className={styles.detailsButton}
                onClick={this.handleOpen}
              >
                {t('buttons.detailsButton')}
              </Button>
            </Toolbar>
          </AppBar>
          <MatchesResultsList
            matchesList={recentsMatchesList}
            matchesListLoading={matchesResultsLoading}
          />
        </Paper>
        <CompletedMatchesPopup
          isOpen={isPopupOpen}
          onClosePopup={this.handleClose}
        >
          <AllMatchesResultsList
            allMatchesList={allMatchesResultsList}
          />
        </CompletedMatchesPopup>
      </div>
    );
  }
}

RecentsMatches.propTypes = {
  t: PropTypes.func,
  recentsMatchesList: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      homePlayingTeam: PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
        linkToLogo: PropTypes.string,
        goals: PropTypes.number,
      }),
      guestTeam: PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
        linkToLogo: PropTypes.string,
        goals: PropTypes.number,
      }),
    }),
  ).isRequired,
  onGetAllMatchesResult: PropTypes.func,
  matchesResultsLoading: PropTypes.bool,
  allMatchesResultsList: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      homePlayingTeam: PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
        linkToLogo: PropTypes.string,
        goals: PropTypes.number,
      }),
      guestTeam: PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string,
        linkToLogo: PropTypes.string,
        goals: PropTypes.number,
      }),
    }),
  ).isRequired,
};

RecentsMatches.defaultProps = {
  t: () => {},
  onGetAllMatchesResult: () => {},
  matchesResultsLoading: false,
};

export default withNamespaces('translation')(RecentsMatches);
