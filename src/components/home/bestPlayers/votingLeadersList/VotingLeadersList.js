import React from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListSubheader from '@material-ui/core/ListSubheader';
import { Avatar } from '@material-ui/core';
import Favorite from '@material-ui/icons/Favorite';
import { withNamespaces } from 'react-i18next';
import styles from './VotingLeadersList.scss';

const VotingLeadersList = ({ allPlayers, matchId, t }) => {
  const leadersCount = 3;
  const players = [...allPlayers]
    .map(({ player }) => Object.assign({}, player, {
      votes: player.stats.find(stat => stat.match === matchId).fansVotes,
    }))
    .sort((a, b) => b.votes - a.votes)
    .slice(0, leadersCount)
    .map(player => (
      <ListItem key={player._id}>
        <Avatar src={player.photoLink} />
        <ListItemText
          className={styles.playerName}
          primary={`${player.number}. ${player.firstName} ${player.lastName}`}
          primaryTypographyProps={{ variant: 'subtitle1' }}
        />
        <ListItemIcon>
          <Favorite nativeColor="#ff3346" />
        </ListItemIcon>
        <ListItemText
          className={styles.numberOfLikes}
          primary={player.votes}
          primaryTypographyProps={{ variant: 'subtitle2', color: 'textSecondary' }}
        />
      </ListItem>
    ));

  return (
    <List
      subheader={(
        <ListSubheader className={styles.leadersListHeader}>
          {t('homePage.bestPlayers.leadersSubheader')}
        </ListSubheader>
      )}
      component="div"
      className={styles.leadersList}
    >
      {players}
    </List>
  );
};

VotingLeadersList.propTypes = {
  allPlayers: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.any)),
  matchId: PropTypes.string,
  t: PropTypes.func,
};

VotingLeadersList.defaultProps = {
  allPlayers: [],
  matchId: '',
  t: () => {},
};

export default withNamespaces('translation')(VotingLeadersList);
