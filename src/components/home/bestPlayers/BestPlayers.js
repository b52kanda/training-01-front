import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loader from 'components/loader/Loader';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withNamespaces } from 'react-i18next';
import AllPlayersList from './allPlayersList/AllPlayersList';
import VotingLeadersList from './votingLeadersList/VotingLeadersList';
import styles from './BestPlayers.scss';

class BestPlayers extends Component {
  componentWillMount() {
    const { onGetLastMatch } = this.props;

    onGetLastMatch();
  }

  render() {
    const {
      lastCompletedMatchInfo,
      lastCompletedMatchInfoLoading,
      onVoteClickServerAction,
      t,
    } = this.props;

    const { teamNames, allPlayers, matchId } = lastCompletedMatchInfo;

    const content = lastCompletedMatchInfoLoading ? (
      <div className={styles.blockLoader}>
        <Loader size="lg" />
      </div>
    ) : (
      <div>
        <AppBar elevation={0} position="static">
          <Toolbar>
            <Typography variant="h6" color="inherit">
              {t('homePage.bestPlayers.blockTitle')}
              {`${teamNames[0]} - ${teamNames[1]}`}
            </Typography>
          </Toolbar>
        </AppBar>
        <div className={styles.listsWrapper}>
          <AllPlayersList
            onVoteClickServerAction={onVoteClickServerAction}
            allPlayers={allPlayers}
            matchId={matchId}
          />
          <VotingLeadersList allPlayers={allPlayers} matchId={matchId} />
        </div>
      </div>
    );

    return (
      <Paper className={styles.blockWrapper} square elevation={1}>
        {content}
      </Paper>
    );
  }
}

BestPlayers.propTypes = {
  onGetLastMatch: PropTypes.func,
  onVoteClickServerAction: PropTypes.func,
  t: PropTypes.func,
  lastCompletedMatchInfo: PropTypes.shape({
    teamNames: PropTypes.array.isRequired,
    allPlayers: PropTypes.array.isRequired,
    matchId: PropTypes.string.isRequired,
  }).isRequired,
  lastCompletedMatchInfoLoading: PropTypes.bool,
};

BestPlayers.defaultProps = {
  onGetLastMatch: () => {},
  onVoteClickServerAction: () => {},
  t: () => {},
  lastCompletedMatchInfoLoading: false,
};

export default withNamespaces('translation')(BestPlayers);
