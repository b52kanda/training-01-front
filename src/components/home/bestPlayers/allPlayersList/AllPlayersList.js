import React from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import VoteButton from 'components/voteButton/VoteButton';
import styles from './AllPlayersList.scss';

const AllPlayersList = ({ allPlayers, onVoteClickServerAction, matchId }) => {
  const players = allPlayers.map(player => (
    <ListItem key={player.player._id}>
      <ListItemText
        primary={`${player.player.number}. ${player.player.firstName} ${player.player.lastName}`}
        primaryTypographyProps={{ variant: 'subtitle2' }}
      />
      <ListItemIcon>
        <VoteButton
          onVoteClickServerAction={onVoteClickServerAction}
          playerId={player.player._id}
          matchId={matchId}
        />
      </ListItemIcon>
    </ListItem>
  ));

  return (
    <List component="div" className={styles.allPlayersList}>
      <Scrollbars renderView={prop => <div {...prop} className={styles.scrollView} />}>
        {players}
      </Scrollbars>
    </List>
  );
};

AllPlayersList.propTypes = {
  onServerVote: PropTypes.func,
  allPlayers: PropTypes.arrayOf(PropTypes.objectOf(PropTypes.any)),
  matchId: PropTypes.string,
};

AllPlayersList.defaultProps = {
  onServerVote: () => {},
  allPlayers: [],
  matchId: '',
};

export default AllPlayersList;
