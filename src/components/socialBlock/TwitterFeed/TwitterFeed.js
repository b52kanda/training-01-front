import React from 'react';
import PropTypes from 'prop-types';
import { TwitterTimelineEmbed } from 'react-twitter-embed';

const TwitterFeed = ({ sourceName, width, height }) => (
  <div>
    <TwitterTimelineEmbed
      sourceType="profile"
      screenName={sourceName}
      noFooter
      noScrollbar
      options={{
        height,
        width,
      }}
    />
  </div>
);

TwitterFeed.propTypes = {
  sourceName: PropTypes.string.isRequired,
  width: PropTypes.number,
  height: PropTypes.number,
};

TwitterFeed.defaultProps = {
  width: 350,
  height: 600,
};

export default TwitterFeed;
