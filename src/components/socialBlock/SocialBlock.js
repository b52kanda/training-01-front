import React from 'react';
import TwitterFeed from './TwitterFeed/TwitterFeed';
import styles from './SocialBlock.scss';

const SocialBlock = () => (
  <div className={styles.socialBlock}>
    <TwitterFeed sourceName="England" />
  </div>
);
export default SocialBlock;
