import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';

import { userAdmin, userCoach } from 'consts';

const RedirectUser = ({ role }) => {
  if (role === userAdmin) {
    return <Redirect to="/admin" />;
  }

  if (role === userCoach) {
    return <Redirect to="/coach" />;
  }

  return <Redirect to="/" />;
};

RedirectUser.propTypes = {
  role: PropTypes.string,
};

RedirectUser.defaultProps = {
  role: '',
};

export default RedirectUser;
