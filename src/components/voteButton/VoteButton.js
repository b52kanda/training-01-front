import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Favorite from '@material-ui/icons/Favorite';
import ButtonBase from '@material-ui/core/ButtonBase';

export default class LikeButton extends Component {
  constructor(props) {
    super(props);

    this.state = { voted: false };
  }

  onVoteClick() {
    this.setState({ voted: true });
  }

  render() {
    const { voted } = this.state;
    const { onVoteClickServerAction, matchId, playerId } = this.props;

    const VoteIcon = voted ? <Favorite nativeColor="#ff3346" /> : <FavoriteBorder />;

    return (
      <ButtonBase
        disabled={voted}
        onClick={() => {
          this.onVoteClick();
          onVoteClickServerAction(playerId, matchId);
        }}
      >
        {VoteIcon}
      </ButtonBase>
    );
  }
}

LikeButton.propTypes = {
  onVoteClickServerAction: PropTypes.func,
  matchId: PropTypes.string,
  playerId: PropTypes.string,
};

LikeButton.defaultProps = {
  onVoteClickServerAction: () => {},
  matchId: '',
  playerId: '',
};
