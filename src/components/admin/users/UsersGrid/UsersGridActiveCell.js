import React from 'react';
import PropTypes from 'prop-types';

import Checkbox from '@material-ui/core/Checkbox';
import TableCell from '@material-ui/core/TableCell';
import Loader from 'components/loader/Loader';

const UsersGridActiveCell = (props) => {
  const { user, onActivateUser } = props;

  if (user.isUpdating) {
    return (
      <TableCell>
        <Loader size="md" />
      </TableCell>
    );
  }

  return (
    <TableCell>
      <Checkbox
        checked={user.isActive}
        disabled={user.isActive}
        onClick={() => onActivateUser(user)}
      />
    </TableCell>
  );
};

UsersGridActiveCell.propTypes = {
  onActivateUser: PropTypes.func,
  user: PropTypes.shape({
    isActive: PropTypes.bool,
  }).isRequired,
};

UsersGridActiveCell.defaultProps = {
  onActivateUser: () => {},
};

export default UsersGridActiveCell;
