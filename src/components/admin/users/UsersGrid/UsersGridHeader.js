import React from 'react';
import { withNamespaces } from 'react-i18next';
import PropTypes from 'prop-types';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';

const UsersGridHeader = (props) => {
  const { t } = props;

  return (
    <TableHead>
      <TableRow>
        <TableCell>
          <Typography variant="subtitle1" color="primary">
            {t('admin.usersGrid.email')}
          </Typography>
        </TableCell>
        <TableCell>
          <Typography variant="subtitle1" color="primary">
            {t('admin.usersGrid.active')}
          </Typography>
        </TableCell>
      </TableRow>
    </TableHead>
  );
};

UsersGridHeader.propTypes = {
  t: PropTypes.func,
};

UsersGridHeader.defaultProps = {
  t: () => {},
};

export default withNamespaces('translation')(UsersGridHeader);
