import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import Loader from 'components/loader/Loader';
import UsersGridBody from './UsersGridBody';
import UsersGridHeader from './UsersGridHeader';
import styles from './usersGrid.scss';

class UsersGrid extends Component {
  componentWillMount() {
    const { onGetUsers } = this.props;

    onGetUsers();
  }

  activateUser({ _id }) {
    const { onActivateUser } = this.props;

    onActivateUser({ id: _id });
  }

  render() {
    const { users, usersLoading } = this.props;

    if (usersLoading) {
      return (
        <div className={styles.gridLoader}>
          <Loader size="lg" />
        </div>
      );
    }

    return (
      <Table>
        <UsersGridHeader />
        <UsersGridBody
          users={users}
          onActivateUser={(...args) => this.activateUser(...args)}
        />
      </Table>
    );
  }
}

UsersGrid.propTypes = {
  onGetUsers: PropTypes.func,
  onActivateUser: PropTypes.func,
  users: PropTypes.arrayOf(PropTypes.object),
  usersLoading: PropTypes.bool,
};

UsersGrid.defaultProps = {
  onGetUsers: () => {},
  onActivateUser: () => {},
  users: [],
  usersLoading: false,
};

export default UsersGrid;
