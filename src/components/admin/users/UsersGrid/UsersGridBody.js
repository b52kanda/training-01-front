import React from 'react';
import PropTypes from 'prop-types';

import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

import UsersGridActiveCell from './UsersGridActiveCell';

const UsersTableBody = (props) => {
  const { users, onActivateUser } = props;

  const userRows = users.map(user => (
    <TableRow key={user._id}>
      <TableCell>{user.email}</TableCell>
      <UsersGridActiveCell user={user} onActivateUser={onActivateUser} />
    </TableRow>
  ));

  return (
    <TableBody>
      {userRows}
    </TableBody>
  );
};

UsersTableBody.propTypes = {
  onActivateUser: PropTypes.func,
  users: PropTypes.arrayOf(PropTypes.object),
};

UsersTableBody.defaultProps = {
  onActivateUser: () => {},
  users: [],
};

export default UsersTableBody;
