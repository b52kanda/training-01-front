import React, { Component } from 'react';
import { withNamespaces } from 'react-i18next';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from './logo.scss';

class Logo extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { t } = this.props;
    return (
      <div className={styles.logo}>
        <Link to="/">
          <img alt={t('labels.logoLabel')} />
        </Link>
      </div>
    );
  }
}
Logo.propTypes = {
  t: PropTypes.func,
};

Logo.defaultProps = {
  t: () => {},
};

export default withNamespaces('translation')(Logo);
