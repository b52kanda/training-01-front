import React from 'react';
import { withNamespaces } from 'react-i18next';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import HeaderMenu from 'components/header/HeaderMenu';

import styles from './Header.scss';

const Header = ({ t, logoutUser, user }) => {
  const isActiveUser = user && user.isActive;

  return (
    <AppBar position="sticky">
      <Toolbar className={styles.header}>
        <Typography variant="h6" color="inherit">
          {t('mainTitle')}
        </Typography>
        {isActiveUser ? (
          <HeaderMenu user={user} logoutUser={logoutUser} />
        ) : (
          <Link className={styles.loginLink} to="/login">{t('buttons.loginButton')}</Link>
        )}
      </Toolbar>
    </AppBar>
  );
};

Header.propTypes = {
  t: PropTypes.func,
  logoutUser: PropTypes.func,
  user: PropTypes.shape({
    isActive: PropTypes.bool,
  }),
};

Header.defaultProps = {
  t: () => {},
  logoutUser: () => {},
  user: null,
};

export default withNamespaces('translation')(Header);
