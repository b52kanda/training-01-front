import React, { Component } from 'react';
import { withNamespaces } from 'react-i18next';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Menu from '@material-ui/core/Menu';
import MenuItemLink from 'components/menuItemLink/MenuItemLink';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';

import styles from './HeaderMenu.scss';

class HeaderMenu extends Component {
  constructor() {
    super();

    this.state = {
      anchorEl: null,
    };
  }

  handleMenu(event) {
    this.setState({ anchorEl: event.currentTarget });
  }

  handleClose() {
    this.setState({ anchorEl: null });
  }

  handleCloseAndLogout() {
    const { logoutUser } = this.props;
    this.handleClose();
    logoutUser();
  }

  render() {
    const { anchorEl } = this.state;
    const { user, t } = this.props;

    return (
      <div className={styles.headerMenu}>
        <IconButton
          color="inherit"
          aria-label="Menu"
          onClick={e => this.handleMenu(e)}
        >
          <Typography className={styles.welcomeTitle} color="inherit" variant="subtitle2">
            {t('welcome', { userEmail: user.email })}
          </Typography>
          <AccountCircle />
        </IconButton>
        <Menu
          id="menu-appbar"
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={!!anchorEl}
          onClose={() => this.handleClose()}
        >
          <MenuItem>{user.role}</MenuItem>
          <MenuItemLink
            to="/"
            onClick={() => this.handleCloseAndLogout()}
          >
            {t('buttons.logoutButton')}
          </MenuItemLink>
        </Menu>
      </div>
    );
  }
}

HeaderMenu.propTypes = {
  t: PropTypes.func,
  user: PropTypes.shape({
    isActive: PropTypes.bool,
  }).isRequired,
  logoutUser: PropTypes.func,
};

HeaderMenu.defaultProps = {
  t: () => {},
  logoutUser: () => {},
};

export default withNamespaces('translation')(HeaderMenu);
