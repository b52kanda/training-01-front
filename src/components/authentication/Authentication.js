import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PageLoader from 'components/pageLoader/PageLoader';

class Authentication extends Component {
  componentWillMount() {
    const { getCurrentUser } = this.props;

    getCurrentUser();
  }

  render() {
    const { children, userLoading } = this.props;

    if (userLoading) {
      return <PageLoader size="lg" />;
    }

    return <div>{ children }</div>;
  }
}

Authentication.propTypes = {
  userLoading: PropTypes.bool.isRequired,
  getCurrentUser: PropTypes.func,
  children: PropTypes.node,
};

Authentication.defaultProps = {
  getCurrentUser: () => {},
  children: [],
};

export default Authentication;
