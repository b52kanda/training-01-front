import React from 'react';
import Loader from 'components/loader/Loader';
import styles from './pageLoader.scss';

export default () => (
  <div className={styles.pageLoaderWrapper}>
    <Loader size="lg" />
  </div>
);
