import React from 'react';
import PropTypes from 'prop-types';

import styles from './loader.scss';

const Loader = ({ size }) => (
  <div className={[styles.loader, size].join(' ')} />
);

Loader.propTypes = {
  size: PropTypes.oneOf(['sm', 'md', 'lg']),
};

Loader.defaultProps = {
  size: 'sm',
};

export default Loader;
