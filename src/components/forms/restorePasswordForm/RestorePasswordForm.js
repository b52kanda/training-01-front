import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField/TextField';
import Button from '@material-ui/core/Button/Button';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import styles from 'components/forms/forms.scss';

class RestorePasswordForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
    };
    this.onInputChange = this.onInputChange.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
  }

  onSubmitForm(event) {
    const { onRestorePassword } = this.props;
    const { email } = this.state;
    event.preventDefault();
    onRestorePassword({
      user: {
        email,
      },
    });

    this.cleanUpForm();
  }

  onInputChange({ target: { name, value } }) {
    this.setState({ [name]: value });
  }

  cleanUpForm() {
    this.setState({
      email: '',
    });
  }

  render() {
    const { t } = this.props;
    const { email } = this.state;
    return (
      <form className={styles.form} onSubmit={(...args) => this.onSubmitForm(...args)}>
        <h1>{t('text.restorePasswordText')}</h1>
        <TextField
          label={t('labels.emailLabel')}
          type="email"
          name="email"
          value={email}
          onChange={this.onInputChange}
          variant="outlined"
          autoFocus
          margin="normal"
          required
        />
        <Button
          className={styles.button}
          type="submit"
          variant="contained"
        >
          {t('buttons.okButton')}
        </Button>
      </form>
    );
  }
}

RestorePasswordForm.propTypes = {
  onRestorePassword: PropTypes.func,
  t: PropTypes.func,
};

RestorePasswordForm.defaultProps = {
  onRestorePassword: () => {},
  t: () => {},
};

export default withNamespaces('translation')(RestorePasswordForm);
