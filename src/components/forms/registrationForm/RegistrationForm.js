import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField/TextField';
import FormControl from '@material-ui/core/FormControl/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel';
import Radio from '@material-ui/core/Radio/Radio';
import Button from '@material-ui/core/Button/Button';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import styles from 'components/forms/forms.scss';
import { userAdmin, userCoach } from 'consts';

class RegistrationForm extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      confirmationPassword: '',
      isPasswordsEqual: true,
      isValidPassword: true,
      role: userAdmin,
    };
    this.regExpForPassword = new RegExp('([\\d]+.*[a-zA-Z]+)|([a-zA-Z]+.*[\\d]+)');
    this.onPasswordFocus = this.onPasswordFocus.bind(this);
    this.onPasswordBlur = this.onPasswordBlur.bind(this);
    this.onConfirmPasswordFocus = this.onConfirmPasswordFocus.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.onConfirmPasswordBlur = this.onConfirmPasswordBlur.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  onInputChange({ target: { name, value } }) {
    this.setState({
      [name]: value,
    });
    if (name === 'password') {
      this.setState({
        confirmationPassword: '',
      });
    }
  }

  onSubmitForm(event) {
    const { onRegister, history } = this.props;
    const { email, password, role } = this.state;
    event.preventDefault();
    onRegister({
      user:
        {
          email,
          password,
          role,
        },
    }, history);
  }

  onPasswordBlur() {
    this.setState({
      isValidPassword: this.isPasswordValid(),
    });
  }

  onPasswordFocus() {
    this.setState({
      isValidPassword: true,
    });
  }

  onConfirmPasswordFocus() {
    this.setState({ isPasswordsEqual: true });
  }

  onConfirmPasswordBlur() {
    this.setState(prevState => ({
      isPasswordsEqual: prevState.password === prevState.confirmationPassword,
    }));
  }

  isPasswordValid() {
    const { password } = this.state;
    return password.length > 5
      && this.regExpForPassword.test(password);
  }

  cleanUpForm() {
    this.setState({
      email: '',
      password: '',
      confirmationPassword: '',
      role: userAdmin,
    });
  }

  handleChange(event) {
    this.setState({ role: event.target.value });
  }

  render() {
    const { t } = this.props;
    const {
      email, role, password, confirmationPassword, isPasswordsEqual, isValidPassword,
    } = this.state;
    return (
      <form className={styles.form} onSubmit={(...args) => this.onSubmitForm(...args)}>
        <h1>{t('text.registrationText')}</h1>
        <TextField
          label={t('labels.emailLabel')}
          type="email"
          name="email"
          value={email}
          onChange={this.onInputChange}
          variant="outlined"
          margin="normal"
          required
        />

        <FormControl component="fieldset" variant="outlined">
          <div className={styles.row}>
            <FormControlLabel value={userAdmin} control={<Radio color="primary" checked={role === userAdmin} onChange={this.handleChange} />} label={t('labels.managerLabel')} />
            <FormControlLabel value={userCoach} control={<Radio color="primary" checked={role === userCoach} onChange={this.handleChange} />} label={t('labels.coachLabel')} />
          </div>
        </FormControl>

        <TextField
          error={!isValidPassword}
          label={t('labels.passwordLabel')}
          type="password"
          name="password"
          value={password}
          onChange={this.onInputChange}
          onBlur={this.onPasswordBlur}
          onFocus={this.onPasswordFocus}
          variant="outlined"
          margin="normal"
          required
          helperText={t('labels.passwordRequirementsLabel')}
        />
        <TextField
          label={t('labels.repeatPasswordLabel')}
          type="password"
          name="confirmationPassword"
          value={confirmationPassword}
          onChange={this.onInputChange}
          variant="outlined"
          margin="normal"
          required
          onBlur={this.onConfirmPasswordBlur}
          error={!isPasswordsEqual}
          onFocus={this.onConfirmPasswordFocus}
        />

        <Button
          className={styles.button}
          type="submit"
          variant="contained"
          disabled={!isPasswordsEqual || !isValidPassword}
        >
          {t('buttons.registrationButton')}
        </Button>
      </form>
    );
  }
}

RegistrationForm.propTypes = {
  onRegister: PropTypes.func,
  t: PropTypes.func,
  history: PropTypes.shape({
    length: PropTypes.number,
  }),
};

RegistrationForm.defaultProps = {
  onRegister: () => {},
  t: () => {},
  history: null,
};

export default withNamespaces('translation')(RegistrationForm);
