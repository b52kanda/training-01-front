import React from 'react';
import Paper from '@material-ui/core/Paper/Paper';
import PropTypes from 'prop-types';
import Logo from 'components/logo/Logo';
import RedirectUser from 'components/redirectUser/RedirectUser';
import styles from './authForm.scss';

const AuthForm = ({ user, children }) => {
  if (user && user.isActive) {
    return (<RedirectUser role={user.role} />);
  }

  return (
    <div className={styles.container}>
      <Paper elevation={5}>
        <div>
          <Logo />
        </div>
        {children}
      </Paper>
    </div>
  );
};

AuthForm.propTypes = {
  children: PropTypes.node.isRequired,
  user: PropTypes.shape({
    email: PropTypes.string,
  }),
};

AuthForm.defaultProps = {
  user: {},
};

export default AuthForm;
