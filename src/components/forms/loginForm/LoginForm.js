import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField/TextField';
import Button from '@material-ui/core/Button/Button';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import styles from 'components/forms/forms.scss';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
  }

  onSubmitForm(event) {
    const { onLogin } = this.props;
    const { email, password } = this.state;
    event.preventDefault();
    onLogin({ user: { email, password } });
    this.cleanUpForm();
  }

  onInputChange({ target: { name, value } }) {
    this.setState({ [name]: value });
  }

  cleanUpForm() {
    this.setState({
      email: '',
      password: '',
    });
  }

  render() {
    const { t } = this.props;
    const { email, password } = this.state;

    return (
      <form className={styles.form} onSubmit={(...args) => this.onSubmitForm(...args)}>
        <TextField
          label={t('labels.emailLabel')}
          type="email"
          name="email"
          value={email}
          onChange={this.onInputChange}
          variant="outlined"
          autoFocus
          margin="normal"
          required
        />
        <TextField
          label={t('labels.passwordLabel')}
          type="password"
          name="password"
          value={password}
          onChange={this.onInputChange}
          variant="outlined"
          margin="normal"
          required
        />
        <Button
          className={styles.button}
          type="submit"
          variant="contained"
        >
          {t('buttons.loginButton')}
        </Button>
      </form>
    );
  }
}

LoginForm.propTypes = {
  onLogin: PropTypes.func,
  t: PropTypes.func,
};

LoginForm.defaultProps = {
  onLogin: () => {},
  t: () => {},
};


export default withNamespaces('translation')(LoginForm);
