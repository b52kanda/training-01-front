import React from 'react';
import BestPlayers from 'containers/home/BestPlayers';
import SocialBlock from 'components/socialBlock/SocialBlock';
import RecentsMatches from 'containers/home/RecentsMatches';
import styles from './home.scss';

export default () => (
  <div>
    Home Page
    <div className={styles.recentsMatches}>
      <RecentsMatches />
      <BestPlayers />
    </div>
    <SocialBlock />

  </div>
);
