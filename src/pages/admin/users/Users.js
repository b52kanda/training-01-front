import React from 'react';
import UsersGrid from 'containers/admin/UsersGrid';

export default () => (
  <UsersGrid />
);
