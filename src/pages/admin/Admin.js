import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Users from './users/Users';
import AdminNoMatch from './AdminNoMatch';

export default () => (
  <Switch>
    <Route exact path="/admin/users" component={Users} />
    <Route component={AdminNoMatch} />
  </Switch>
);
