import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import styles from 'pages/authorization/authStyle.scss';
import AuthForm from 'containers/forms/AuthForm';
import RestorePasswordForm from 'containers/forms/RestorePasswordForm';
import { Link } from 'react-router-dom';

class RestorePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { t } = this.props;
    return (
      <div className={styles.wrapper}>
        <AuthForm>
          <RestorePasswordForm />
          <div className={styles.links}>
            <Link to="/">{t('labels.homeLabel')}</Link>
            <Link to="/login">{t('labels.loginLabel')}</Link>
          </div>
        </AuthForm>
      </div>
    );
  }
}

RestorePassword.propTypes = {
  t: PropTypes.func,
};

RestorePassword.defaultProps = {
  t: () => {},
};

export default withNamespaces('translation')(RestorePassword);
