import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withNamespaces } from 'react-i18next';
import { Link } from 'react-router-dom';
import styles from 'pages/authorization/authStyle.scss';
import AuthForm from 'containers/forms/AuthForm';
import RegistrationForm from 'containers/forms/RegistrationForm';

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { t } = this.props;
    return (
      <div className={styles.wrapper}>
        <AuthForm>
          <RegistrationForm />
          <div className={styles.links}>
            <Link to="/">{t('labels.homeLabel')}</Link>
            <Link to="/login">{t('labels.loginLabel')}</Link>
          </div>
        </AuthForm>
      </div>
    );
  }
}

Registration.propTypes = {
  t: PropTypes.func,
};

Registration.defaultProps = {
  t: () => {},
};

export default withNamespaces('translation')(Registration);
