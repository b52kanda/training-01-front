import React, { Component } from 'react';
import { withNamespaces } from 'react-i18next';
import PropTypes from 'prop-types';
import AuthForm from 'containers/forms/AuthForm';
import styles from 'pages/authorization/authStyle.scss';
import LoginForm from 'containers/forms/LoginForm';
import { Link } from 'react-router-dom';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { t } = this.props;
    return (
      <div className={styles.wrapper}>
        <AuthForm>
          <LoginForm />
          <div className={styles.links}>
            <Link to="/restore">{t('labels.restorePasswordLabel')}</Link>
            <Link to="/registration">{t('labels.registrationLabel')}</Link>
          </div>
        </AuthForm>
      </div>);
  }
}

Login.propTypes = {
  t: PropTypes.func,
};

Login.defaultProps = {
  t: () => {},
};

export default withNamespaces('translation')(Login);
