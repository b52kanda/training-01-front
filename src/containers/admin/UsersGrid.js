import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUsers, activateUser } from 'actions/usersActions';
import UsersGrid from 'components/admin/users/UsersGrid/UsersGrid';

const mapStateToProps = state => ({
  users: state.users,
  usersLoading: state.usersLoading,
});

const mapActionsToProps = dispatch => ({
  onGetUsers: bindActionCreators(getUsers, dispatch),
  onActivateUser: bindActionCreators(activateUser, dispatch),
});

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(UsersGrid);
