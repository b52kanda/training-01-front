import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Header from 'components/header/Header';
import { bindActionCreators } from 'redux';
import { logoutUser } from 'actions/userActions';

const mapStateToProps = state => ({
  user: state.user,
});

const mapActionsToProps = dispatch => ({
  logoutUser: bindActionCreators(logoutUser, dispatch),
});

export default withRouter(connect(mapStateToProps, mapActionsToProps)(Header));
