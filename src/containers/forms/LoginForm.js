import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import LoginForm from 'components/forms/loginForm/LoginForm';
import { loginUser } from 'actions/userActions';

const mapActionsToProps = dispatch => ({
  onLogin: bindActionCreators(loginUser, dispatch),
});

export default connect(null, mapActionsToProps)(LoginForm);
