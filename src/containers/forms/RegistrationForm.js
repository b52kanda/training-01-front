import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import RegistrationForm from 'components/forms/registrationForm/RegistrationForm';
import { registerUser } from 'actions/userActions';
import { withRouter } from 'react-router-dom';

const mapStateToProps = state => ({
  user: state.user,
});

const mapActionsToProps = dispatch => ({
  onRegister: bindActionCreators(registerUser, dispatch),
});

export default withRouter(connect(mapStateToProps, mapActionsToProps)(RegistrationForm));
