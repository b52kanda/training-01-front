import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import RestorePasswordForm from 'components/forms/restorePasswordForm/RestorePasswordForm';
import { resetPassword } from 'actions/userActions';

const mapActionsToProps = dispatch => ({
  onRestorePassword: bindActionCreators(resetPassword, dispatch),
});

export default connect(null, mapActionsToProps)(RestorePasswordForm);
