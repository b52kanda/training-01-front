import { connect } from 'react-redux';
import AuthForm from 'components/forms/authForm/AuthForm';

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(AuthForm);
