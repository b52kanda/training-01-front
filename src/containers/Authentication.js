import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import Authentication from 'components/authentication/Authentication';
import { getCurrentUser } from 'actions/userActions';

const mapStateToProps = state => ({
  userLoading: state.userLoading,
});

function mapActionsToProps(dispatch) {
  return {
    getCurrentUser: bindActionCreators(getCurrentUser, dispatch),
  };
}

export default withRouter(connect(mapStateToProps, mapActionsToProps)(Authentication));
