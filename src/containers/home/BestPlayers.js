import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import BestPlayers from 'components/home/bestPlayers/BestPlayers';
import {
  getLastCompletedMatchInfo,
  addVoteByPlayerAndMatchIds,
} from 'actions/lastCompletedMatchActions';

const mapStateToProps = state => ({
  lastCompletedMatchInfo: state.lastCompletedMatchInfo,
  lastCompletedMatchInfoLoading: state.lastCompletedMatchInfoLoading,
});

const mapActionsToProps = dispatch => ({
  onGetLastMatch: bindActionCreators(getLastCompletedMatchInfo, dispatch),
  onVoteClickServerAction: bindActionCreators(addVoteByPlayerAndMatchIds, dispatch),
});

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(BestPlayers);
