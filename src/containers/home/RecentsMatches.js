import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import RecentsMatches from 'components/home/recentsMatches/RecentsMatches';
import {
  getAllMatchesResults,
} from 'actions/matchesResultsActions';

const mapStateToProps = state => ({
  recentsMatchesList: state.matchesResults.slice(0, 5),
  matchesResultsLoading: state.matchesResultsLoading,
  allMatchesResultsList: state.matchesResults,
});

const mapActionsToProps = dispatch => ({
  onGetAllMatchesResult: bindActionCreators(getAllMatchesResults, dispatch),
});

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(RecentsMatches);
