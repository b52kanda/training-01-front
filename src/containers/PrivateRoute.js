import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PrivateRoute from 'components/privateRoute/PrivateRoute';

const mapStateToProps = state => ({
  user: state.user,
  userLoading: state.userLoading,
});

export default withRouter(connect(mapStateToProps)(PrivateRoute));
