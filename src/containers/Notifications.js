import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { closeNotification } from 'actions/notificationActions';
import Notifications from 'components/notifications/Notifications';

const mapStateToProps = state => ({
  notifications: state.notifications,
});

const mapActionsToProps = dispatch => ({
  onCloseNotification: bindActionCreators(closeNotification, dispatch),
});

export default connect(mapStateToProps, mapActionsToProps)(Notifications);
