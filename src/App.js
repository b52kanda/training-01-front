import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';

import Content from 'components/content/Content';
import Authentication from './containers/Authentication';
import Notifications from './containers/Notifications';
import Header from './containers/Header';
import store from './store';

export default () => (
  <Provider store={store}>
    <Router>
      <Authentication>
        <Notifications />
        <Header />
        <Content />
      </Authentication>
    </Router>
  </Provider>
);
