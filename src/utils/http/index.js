import appConfig from 'config';
import ServerError from './ServerError';

export default class {
  static get AuthHeader() {
    const token = localStorage.getItem('id_token');

    return token ? `Token ${token}` : null;
  }

  static get ApiUrl() {
    return `${appConfig.API_URL}/api`;
  }

  static async fetch(resourceUrl, fetchConfig = {}) {
    const url = `${this.ApiUrl}${resourceUrl}`;
    const config = {
      ...fetchConfig,
      headers: {
        Accept: 'application/json',
        Authorization: this.AuthHeader,
        ...(fetchConfig.headers || {}),
      },
    };
    const response = await fetch(url, config);
    const payload = await response.json();

    if (!response.ok) {
      throw new ServerError(payload);
    }

    return payload;
  }

  static get(url) {
    return this.fetch(url, { method: 'GET' });
  }

  static post(url, data) {
    return this.fetch(url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data),
    });
  }

  static put(url, data) {
    return this.fetch(url, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data),
    });
  }

  static patch(url, data) {
    return this.fetch(url, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data),
    });
  }

  static delete(url) {
    return this.fetch(url, { method: 'DELETE' });
  }
}
