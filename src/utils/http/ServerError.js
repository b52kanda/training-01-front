export default class extends Error {
  constructor(payload) {
    super(payload.errors.map(error => error.message).join(', '));

    this.errors = payload.errors;
  }
}
